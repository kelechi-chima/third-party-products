#!/bin/bash

# This script performs the following:
#  - update centos
#  - update the timezone
#  - start the firewall manager
#  - set selinux to permissive mode to allow rabbitmq to start up
#  - ensures that the wget utility is present
#  - downloads and installs erlang, a rabbitmq dependency
#  - downloads and installs rabbitmq-server
#  - enables the rabbitmq management plugin
#  - creates an admin user to drive the import definitions process
#  - imports the rabbitmq definitions into the running rabbitmq server

sudo yum update -y
sudo timedatectl set-timezone Europe/London

# this sets selinux to permissive mode, probably not sensible
sudo setenforce 0

# Need to set selinux boot-time config as well as above runtime setting does not affect it
selinux_config=/etc/selinux/config
[[ -f ${selinux_config} ]] && sudo sed --in-place=.bak 's/SELINUX=enforcing/SELINUX=permissive/' ${selinux_config}

sudo yum install wget -y

erlang_rpm=erlang-17.4-1.el6.x86_64.rpm
wget https://www.rabbitmq.com/releases/erlang/${erlang_rpm}
sudo yum install ${erlang_rpm} -y

rabbitmq_rpm=rabbitmq-server-3.5.0-1.noarch.rpm
wget https://www.rabbitmq.com/releases/rabbitmq-server/v3.5.0/${rabbitmq_rpm}
sudo rpm --import http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo yum install ${rabbitmq_rpm} -y

# run rabbitmq on startup
sudo chkconfig rabbitmq-server on

sudo chown -R rabbitmq:rabbitmq /etc/rabbitmq/

# This resolves an issue where rabbitmq settings disappear after restart, due to dynamic IP addresses.
# See https://techblog.willshouse.com/2013/02/27/rabbitmq-users-disappear-settings-gone-after-reboot-restart/
# See http://www.rabbitmq.com/ec2.html
rabbitmq_config=/etc/rabbitmq/rabbitmq-env.conf
sudo touch ${rabbitmq_config}
sudo chmod o+w ${rabbitmq_config}
printf 'NODENAME=rabbit@localhost' >> ${rabbitmq_config}
sudo chmod o-w ${rabbitmq_config}
sudo chown rabbitmq:rabbitmq ${rabbitmq_config}

sudo /sbin/service rabbitmq-server start
sudo rabbitmq-plugins enable rabbitmq_management
sudo rabbitmqctl delete_user guest
sudo rabbitmqctl add_user admin-user password
sudo rabbitmqctl set_user_tags admin-user administrator

# python script that provides the management command line tool
[[ -f rabbitmqadmin ]] || wget https://raw.githubusercontent.com/rabbitmq/rabbitmq-management/rabbitmq_v3_5_0/bin/rabbitmqadmin
chmod +x rabbitmqadmin
sudo ./rabbitmqadmin --username=admin-user --password=password import definitions.json

# clean up
rm ${erlang_rpm}
rm ${rabbitmq_rpm}