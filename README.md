# third-party-products #

This repository holds materials used by the go continuous delivery tool for running the third party product pipelines.

### rabbitmq ###

* JSON file that contains rabbitmq definitions (users, permissions, etc) that will be imported into a rabbitmq instance
* a shell script that will validate the JSON file (note that this assumes that python is available in the go environment)